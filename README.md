# Marvel

Marvel app is a single app with the goal of show the best practices for Swift that Guimel knows as iOS Developer.
This app is developed with the architecture VIPER, using the next template created by Guimel

Look the Xcode template [https://github.com/GuimelGMC/Template_VIPER.git](https://github.com/GuimelGMC/Template_VIPER.git)

***

## Checklist

- [X] Create project
- [X] Create files structure basic
- [X] Create the Home module with VIPER
- [X] Integrate Cocoapods
- [X] Integrate DataService using Alamofire
- [X] Create Models for the Home Module
- [X] Add in HomeInteractor the requesto to get characters pagined way
- [X] Testing Characters service
- [X] Create the CharacterDetail module with VIPER
- [X] Create Models for the CharacterDetail module
- [X] Testing CharacterDetail service
- [X] Layout Home Screen and connect with the logic
- [X] Add new image for Launch Screen
- [X] Search of Characters
- [X] Layout Character Detail and connect with the logic
- [X] Add app Icon
- [X] Add module for show content (Comics and Series)
- [X] Layout of content with two XIB files 
- [X] Upload to Testflight

## Design System
#### Created: Jocelyn Perez

<img src= "images/designsystem.png" height= "480">

[https://www.figma.com/file/2OnvZouNEMTNS9hpTDVjcD/Avengers?node-id=1%3A2&t=s4uOdAXMqG38Nzlu-1](https://www.figma.com/file/2OnvZouNEMTNS9hpTDVjcD/Avengers?node-id=1%3A2&t=s4uOdAXMqG38Nzlu-1)

## VIPER

The goal in this app, is that you can show how to I use the VIPER pattern, I am using a files structure that it can help to understand easily which is the responsability for each layer.

- Module is specifically to init the dependencies of this module, with his business logic, view and router.
- DataManager has the responsability to call the services and receipt the response for send this data to the interactor.
- Interactor has all business logic
- Presenter is an intermediary between the view, interactor and router
- View has all the logic to the layout and show the data

My goal with this, is that every developer can undestand and learn the function for each layer, this is the key to VIPER has success in the project. 

## Testflight

Download the app with the next link: [https://testflight.apple.com/join/Wz2jhGyU](https://testflight.apple.com/join/Wz2jhGyU)

## Postman

- Enviroment: [https://gitlab.com/Guimel_GMC/marvel/-/blob/main/Postman/MArvel_Enviroment.json](https://gitlab.com/Guimel_GMC/marvel/-/blob/main/Postman/MArvel_Enviroment.json)
- Collection: [https://gitlab.com/Guimel_GMC/marvel/-/blob/main/Postman/MArvel_Enviroment.json](https://gitlab.com/Guimel_GMC/marvel/-/blob/main/Postman/MArvel_Enviroment.json)

## References

- [https://medium.com/swift-india/viper-architecture-example-in-ios-in-swift-4-6f656a441f7c](https://medium.com/swift-india/viper-architecture-example-in-ios-in-swift-4-6f656a441f7c)
- [https://developer.apple.com/documentation/dispatch/dispatchgroup](https://developer.apple.com/documentation/dispatch/dispatchgroup)
- [https://dev.to/fmo91/dispatchgroup-in-swift-gg7](https://dev.to/fmo91/dispatchgroup-in-swift-gg7)
- [https://github.com/Alamofire/Alamofire](https://github.com/Alamofire/Alamofire)
- [https://github.com/onevcat/Kingfisher](https://github.com/onevcat/Kingfisher)
- [https://github.com/Swiftify-Corp/IHProgressHUD](https://github.com/Swiftify-Corp/IHProgressHUD)
