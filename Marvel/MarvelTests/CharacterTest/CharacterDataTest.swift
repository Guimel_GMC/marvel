//
//  HomeDataManagerTest.swift
//  MarvelTests
//
//  Created by Guimel Moreno on 02/12/22.
//

import XCTest
import Foundation

@testable import Marvel

final class CharacterDataTest: XCTestCase {
    
    private var dataManager: CharacterDataManagerProtocol!
    private var characterResponse: DataResponse<CharacterModel>?
    private var errorResponse: Error?
    private var character: CharacterModel?
    private var currentComics: [ContentModel]?
    private var currentSeries: [SerieModel]?
    private var idCharacter: Int = 1011334
    
    override func setUp() {
        super.setUp()
        dataManager = CharacterDataManager()
    }

    func test_getComicDetail() {
        let group = DispatchGroup()
        dataManager.requestToGetComicsWith(idCharacter: idCharacter) { character in
            self.character = character
            
            if let endpointComic = character.comics?.collectionURI {
                group.enter()
                self.dataManager.requestToGetComicInformation(endpoint: endpointComic) { comics in
                    self.currentComics = comics
                    group.leave()
                } onError: { error in
                    debugPrint(error.localizedError)
                    XCTAssertNil(error)
                }
            }
            
            if let endpointSeries = character.series?.collectionURI {
                group.enter()
                self.dataManager.requestToGetSeriesInformation(endpoint: endpointSeries) { series in
                    self.currentSeries = series
                    group.leave()
                } onError: { error in
                    XCTAssertNil(error)
                }
            }
            
        } onError: { error in
            XCTAssertNil(error)
            group.leave()
        }
        
        group.notify(queue: .main) {
            XCTAssertNotNil(self.character)
            XCTAssertNotNil(self.currentComics)
            XCTAssertNotNil(self.currentSeries)
        }
    }
}
