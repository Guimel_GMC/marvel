//
//  HomeDataManagerTest.swift
//  MarvelTests
//
//  Created by Guimel Moreno on 02/12/22.
//

import XCTest
import Foundation

@testable import Marvel

final class HomeDataTest: XCTestCase {
    
    private var dataManager: HomeDataManagerProtocol!
    private var characterResponse: DataResponse<CharacterModel>?
    private var errorResponse: RequestError?
    private var responseExpectation: XCTestExpectation?
    private var limit = 10
    private var offset = 0
    private var characters: [CharacterModel] = []
    
    override func setUp() {
        super.setUp()
        dataManager = HomeDataManager()
        responseExpectation = expectation(description: "Character Response Expectation")
    }

    func test_getCharactersPagined() {
        offset += 1
        limit = 20
        dataManager.requestToGetCharacters(offset: offset, limit: limit) { response in
            self.characterResponse = response
            self.characters.append(contentsOf: response.results)
            self.responseExpectation?.fulfill()
        } onError: { error in
            self.errorResponse = error
            self.responseExpectation?.fulfill()
        }
        
        waitForExpectations(timeout: 10)
        
        XCTAssertTrue(characters.count == 20, "Pagined OK")
        XCTAssertTrue(errorResponse == nil, "Request Successfully")
    }
    
    func test_getCharactersService() {
        dataManager.requestToGetCharacters(offset: offset, limit: limit) { response in
            self.characterResponse = response
            self.characters.append(contentsOf: response.results)
            self.responseExpectation?.fulfill()
        } onError: { error in
            self.errorResponse = error
            self.responseExpectation?.fulfill()
        }
        
        waitForExpectations(timeout: 10)
        
        XCTAssertNotNil(characterResponse, "\(characterResponse.debugDescription)")
        XCTAssertTrue(errorResponse == nil, "Request Successfully")
    }
}
