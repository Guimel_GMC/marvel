//
//  UIFont+Tipography.swift
//  Marvel
//
//  Created by Guimel Moreno on 02/12/22.
//

import Foundation
import UIKit

protocol TypographyStylable {
    func setTextStyle(_ style: Typography)
}

extension UILabel: TypographyStylable {
    func setTextStyle(_ style: Typography) {
        textColor = style.color
        font = style.asFont()
        textAlignment = style.alignment
    }
}

extension UITextView: TypographyStylable {
    func setTextStyle(_ style: Typography) {
        textColor = style.color
        font = style.asFont()
        textAlignment = style.alignment
    }
}

struct Typography: Hashable {
    var size: CGFloat
    var lineHeight: CGFloat
    var color: UIColor = .marvelBlack
    var alignment: NSTextAlignment = .left
    var weight: UIFont.Weight
}

extension Typography {
    func asFont() -> UIFont {
        switch weight {
        case .medium: return UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Medium", size: size) ?? .systemFont(ofSize: size, weight: .medium))
        case .regular: return UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Regular", size: size) ?? .systemFont(ofSize: size, weight: .regular))
        default: return .systemFont(ofSize: size, weight: .regular)
        }
        
    }
    
    func withWeight(_ weight: UIFont.Weight) -> Typography {
        var result = self
        result.weight = weight
        return result
    }
    
    func withColor(_ color: UIColor) -> Typography {
        var result = self
        result.color = color
        return result
    }
    
    func withSize(_ size: CGFloat) -> Typography {
        var result = self
        result.size = size
        return result
    }
    
    func withAlignment(_ alignment: NSTextAlignment) -> Typography {
        var result = self
        result.alignment = alignment
        return result
    }
    
    func withLineHeight(_ lineHeight: CGFloat) -> Typography {
        var result = self
        result.lineHeight = lineHeight
        return result
    }
}

extension Typography {
    enum Style {
        static let headingsH1 = Typography(size: 48,
                                           lineHeight: 1.0,
                                           color: .marvelWhite,
                                           alignment: .center,
                                           weight: .medium)
        
        static let headingsH2 = Typography(size: 24,
                                           lineHeight: 1.0,
                                           color: .marvelWhite,
                                           alignment: .left,
                                           weight: .medium)
        
        static let headingsH3 = Typography(size: 16,
                                           lineHeight: 1.0,
                                           color: .marvelWhite,
                                           alignment: .center,
                                           weight: .medium)
        
        static let bodyMedium = Typography(size: 14,
                                            lineHeight: 1.0,
                                           color: .marvelWhite.withAlphaComponent(0.7),
                                            alignment: .left,
                                            weight: .medium)
        static let bodyRegular = Typography(size: 14,
                                            lineHeight: 1.0,
                                            color: .marvelWhite,
                                            alignment: .left,
                                            weight: .regular)
        
        static let bodySmall = Typography(size: 12,
                                          lineHeight: 1.0,
                                          color: .marvelWhite,
                                          alignment: .left,
                                          weight: .regular)
    }
}
