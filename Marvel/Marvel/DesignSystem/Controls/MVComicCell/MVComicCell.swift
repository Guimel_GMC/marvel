//
//  MVComicCell.swift
//  Marvel
//
//  Created by Guimel Moreno on 06/12/22.
//

import Foundation
import UIKit
import Kingfisher

protocol MVComicCellProtocol {
    static var reuseIdentifier: String { get }
    func configureWith(content: ContentModel)
}

class MVComicCell: UICollectionViewCell, MVComicCellProtocol {
    static var reuseIdentifier: String = String(describing: MVComicCell.self)
    
    private lazy var imageView: UIImageView = {
        let imgView = UIImageView(frame: .zero)
        imgView.translatesAutoresizingMaskIntoConstraints = false
        imgView.contentMode = .scaleAspectFill
        return imgView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.setTextStyle(.Style.bodySmall)
        label.adjustsFontSizeToFitWidth = true
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var subtitleLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.setTextStyle(.Style.bodySmall)
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var separator: UIView = {
        let view = UIView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .marvelRed
        return view
    }()
    
    private lazy var contentLabels: UIView = {
        let view = UIView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .marvelGray
        return view
    }()
    
    private var model: ContentModel?
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureWith(content: ContentModel) {
        self.model = content
        imageView.kf.setImage(with: content.thumbnail?.fullPath)
        titleLabel.text = content.title
        subtitleLabel.text = content.headerType == .comics ? "hola" : nil
        
        applyConstraints()
    }
}

private extension MVComicCell {
    func setupView() {
        layer.cornerRadius = 4
        clipsToBounds = true
        contentView.backgroundColor = .lightGray
        contentView.addSubview(imageView)
        contentLabels.addSubview(titleLabel)
        
        contentView.addSubview(contentLabels)
        contentView.addSubview(separator)
        
       
    }
    
    func applyConstraints() {
        NSLayoutConstraint.activate([
            contentLabels.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            contentLabels.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            contentLabels.rightAnchor.constraint(equalTo: contentView.rightAnchor),
            contentLabels.heightAnchor.constraint(equalToConstant: model?.headerType == .comics ? 60 : 42),
            
            separator.bottomAnchor.constraint(equalTo: contentLabels.topAnchor),
            separator.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            separator.rightAnchor.constraint(equalTo: contentView.rightAnchor),
            separator.heightAnchor.constraint(equalToConstant: 2),
            
            imageView.bottomAnchor.constraint(equalTo: separator.topAnchor),
            imageView.topAnchor.constraint(equalTo: contentView.topAnchor),
            imageView.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            imageView.rightAnchor.constraint(equalTo: contentView.rightAnchor)
        ])
        
        if model?.headerType == .comics {
            contentLabels.addSubview(subtitleLabel)
            NSLayoutConstraint.activate([
                subtitleLabel.bottomAnchor.constraint(equalTo: contentLabels.bottomAnchor, constant: -8),
                subtitleLabel.leftAnchor.constraint(equalTo: contentLabels.leftAnchor, constant: 6),
                subtitleLabel.rightAnchor.constraint(equalTo: contentLabels.rightAnchor, constant: -6),
                subtitleLabel.heightAnchor.constraint(equalToConstant: 13)
            ])
        }
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: contentLabels.topAnchor, constant: 4),
            titleLabel.leftAnchor.constraint(equalTo: contentLabels.leftAnchor, constant: 6),
            titleLabel.rightAnchor.constraint(equalTo: contentLabels.rightAnchor, constant: -6),
            titleLabel.bottomAnchor.constraint(equalTo: model!.headerType == .comics ? subtitleLabel.topAnchor : contentLabels.bottomAnchor,
                                               constant: model!.headerType == .comics ? -4 : -8),
        ])
    }
}
