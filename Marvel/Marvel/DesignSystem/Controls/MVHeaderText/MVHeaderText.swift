//
//  MVHeaderText.swift
//  Marvel
//
//  Created by Guimel Moreno on 06/12/22.
//

import Foundation
import UIKit

class MVHeaderText: UICollectionReusableView, MVHeaderProtocol {
    static var elementKind: String = "header_text"
    static var reuseIdentifier: String = String(describing: MVHeaderText.self)
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.setTextStyle(.Style.headingsH2)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor = .marvelBlack
        addSubview(titleLabel)
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: 29),
            titleLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 24),
            titleLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -24),
            titleLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -13),
            titleLabel.heightAnchor.constraint(equalToConstant: 28)
        ])
    }
    
    func configureWith(title: String) {
        titleLabel.text = title
    }
}
