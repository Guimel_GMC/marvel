//
//  MVHeaderCharacter.swift
//  Marvel
//
//  Created by Guimel Moreno on 05/12/22.
//

import Foundation
import UIKit
import Kingfisher

protocol MVHeaderProtocol {
    static var reuseIdentifier: String { get }
    static var elementKind: String { get }
}

protocol MVHeaderCharacterDelegate {
    func openWebBrowserWith(url: URL?)
}
class MVHeaderCharacter: UICollectionReusableView, MVHeaderProtocol {
    static var elementKind: String = "character"
    static var reuseIdentifier: String = String(describing: MVHeaderCharacter.self)
    
    private lazy var headerImage: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.alpha = 0.3
        imageView.backgroundColor = .marvelBlack
        return imageView
    }()
    
    private lazy var title: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.setTextStyle(.Style.headingsH1)
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var subtitle: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.setTextStyle(.Style.headingsH3)
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var newsButton: MVCircularButton = {
        let btn = MVCircularButton(icon: .news)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.handler = openUrl(sender:)
        return btn
    }()
    
    private lazy var externalLinkButton: MVCircularButton = {
        let btn = MVCircularButton(icon: .externalLink)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.handler = openUrl(sender:)
        return btn
    }()
    
    private lazy var contentButtons: UIView = {
        let view = UIView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private var urlNews: URL?
    private var externalLink: URL?
    private var delegate: MVHeaderCharacterDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupHeader(character: CharacterModel?, delegate: MVHeaderCharacterDelegate?) {
        title.text = character?.name
        subtitle.text = character?.description
        headerImage.kf.setImage(with: character?.thumbnail?.fullPath)
        self.delegate = delegate
        
        character?.urls?.forEach({ url in
            switch url.type {
            case .wiki: urlNews = URL(string: url.url ?? "")
            case .comiclink: externalLink = URL(string: url.url ?? "")
            default: break
            }
        })
    }
}

private extension MVHeaderCharacter {
    func openUrl(sender: MVCircularButton) {
        switch sender.buttonIcon {
        case .news: delegate?.openWebBrowserWith(url: urlNews)
        case .externalLink: delegate?.openWebBrowserWith(url: externalLink)
        }
    }
    
    func setupView() {
        addSubview(headerImage)
        addSubview(title)
        addSubview(subtitle)
        
        contentButtons.addSubview(newsButton)
        contentButtons.addSubview(externalLinkButton)
        addSubview(contentButtons)
        
        NSLayoutConstraint.activate([
            headerImage.topAnchor.constraint(equalTo: topAnchor),
            headerImage.leftAnchor.constraint(equalTo: leftAnchor),
            headerImage.rightAnchor.constraint(equalTo: rightAnchor),
            headerImage.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            contentButtons.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -29),
            contentButtons.centerXAnchor.constraint(equalTo: centerXAnchor),
            contentButtons.heightAnchor.constraint(equalToConstant: 51),
            contentButtons.widthAnchor.constraint(equalToConstant: 116),
            
            newsButton.centerYAnchor.constraint(equalTo: contentButtons.centerYAnchor),
            newsButton.leftAnchor.constraint(equalTo: contentButtons.leftAnchor),
            newsButton.heightAnchor.constraint(equalToConstant: 51),
            newsButton.widthAnchor.constraint(equalToConstant: 51),
            
            externalLinkButton.centerYAnchor.constraint(equalTo: contentButtons.centerYAnchor),
            externalLinkButton.rightAnchor.constraint(equalTo: contentButtons.rightAnchor),
            externalLinkButton.heightAnchor.constraint(equalToConstant: 51),
            externalLinkButton.widthAnchor.constraint(equalToConstant: 51),
            subtitle.bottomAnchor.constraint(equalTo: contentButtons.topAnchor, constant: -29),
            subtitle.leftAnchor.constraint(equalTo: leftAnchor, constant: 24),
            subtitle.rightAnchor.constraint(equalTo: rightAnchor, constant: -24),
            title.bottomAnchor.constraint(equalTo: subtitle.topAnchor, constant: -8),
            title.leftAnchor.constraint(equalTo: leftAnchor, constant: 24),
            title.rightAnchor.constraint(equalTo: rightAnchor, constant: -24),
            title.topAnchor.constraint(greaterThanOrEqualTo: topAnchor, constant: 24)
        ])
    }
}
