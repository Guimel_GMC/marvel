//
//  MVCircularButton.swift
//  Marvel
//
//  Created by Guimel Moreno on 05/12/22.
//

import Foundation
import UIKit

enum MVCircularButtonIcons {
    case externalLink
    case news
    
    func getIcon() -> UIImage {
        switch self {
        case .externalLink: return Style.iconCatalog.externalLink
        case .news: return Style.iconCatalog.news
        }
    }
}

class MVCircularButton: UIControl {
    var buttonIcon: MVCircularButtonIcons {
        return icon
    }
    private var icon: MVCircularButtonIcons
    private lazy var imgIcon: UIImageView = {
        let imgView = UIImageView()
        imgView.contentMode = .center
        imgView.tintColor = .marvelWhite
        imgView.translatesAutoresizingMaskIntoConstraints = false
//        imgView.isUserInteractionEnabled = true
        return imgView
    }()
    
    var handler: ((MVCircularButton) -> Void)?
    
    init(icon: MVCircularButtonIcons) {
        self.icon = icon
        super.init(frame: .zero)
        isUserInteractionEnabled = true
        accessibilityTraits = UIAccessibilityTraits.button
        isAccessibilityElement = true
        translatesAutoresizingMaskIntoConstraints = false
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        imgIcon.image = icon.getIcon().withRenderingMode(.alwaysTemplate)
        backgroundColor = .marvelWhite.withAlphaComponent(0.2)
        
        clipsToBounds = true
        addSubview(imgIcon)
        
        NSLayoutConstraint.activate([
            imgIcon.topAnchor.constraint(equalTo: topAnchor),
            imgIcon.leftAnchor.constraint(equalTo: leftAnchor),
            imgIcon.rightAnchor.constraint(equalTo: rightAnchor),
            imgIcon.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        addGestureRecognizer(tapGesture)
//        
//        addTarget(self, action: #selector(handleTap(_:)), for: .allEvents)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = bounds.height / 2
        layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMinXMinYCorner]
    }
    
    @objc
    private func handleTap(_ gesture: UITapGestureRecognizer) {
        handler?(self)
        sendActions(for: .allEvents)
    }
}
