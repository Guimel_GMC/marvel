//
//  MVCharacterCell.swift
//  Marvel
//
//  Created by Guimel Moreno on 04/12/22.
//

import Foundation
import UIKit
import Kingfisher

protocol MVCharacterCellProtocol {
    static var reuseIdentifier: String { get }
    func configureCellWith(character: CharacterModel)
}

class MVCharacterCell: UICollectionViewCell, MVCharacterCellProtocol {
    static var reuseIdentifier: String = String(describing: MVCharacterCell.self)
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.backgroundColor = .marvelGray
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    lazy var contentLabel: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .marvelBlack.withAlphaComponent(0.8)
        return view
    }()
    
    lazy var nameLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.setTextStyle(.Style.bodySmall)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupCell()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupCell()
    }
    
    private func setupCell() {
        contentView.addSubview(imageView)
        contentView.addSubview(contentLabel)
        contentLabel.addSubview(nameLabel)
        layer.cornerRadius = 20
        clipsToBounds = true
        
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: contentView.topAnchor),
            imageView.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            imageView.rightAnchor.constraint(equalTo: contentView.rightAnchor),
            imageView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            contentLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            contentLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor),
            contentLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            contentLabel.heightAnchor.constraint(equalToConstant: 30),
            nameLabel.bottomAnchor.constraint(equalTo: contentLabel.bottomAnchor),
            nameLabel.leftAnchor.constraint(equalTo: contentLabel.leftAnchor, constant: 16),
            nameLabel.rightAnchor.constraint(equalTo: contentLabel.rightAnchor, constant: -16),
            nameLabel.topAnchor.constraint(equalTo: contentLabel.topAnchor)
        ])
    }
    
    func configureCellWith(character: CharacterModel) {
        imageView.kf.setImage(with: character.thumbnail?.fullPath)
        nameLabel.text = character.name
    }
}
