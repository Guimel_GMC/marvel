//
//  IconCatalog.swift
//  Marvel
//
//  Created by Guimel Moreno on 02/12/22.
//

import Foundation
import UIKit

struct IconCatalog {
    let arrowBackward: UIImage = UIImage(systemName: "arrow.backward") ?? UIImage()
    let marvenLogo: UIImage = UIImage(named: "marvel_logo") ?? UIImage()
    let news: UIImage = UIImage(named: "News") ?? UIImage()
    let externalLink: UIImage = UIImage(named: "external_link") ?? UIImage()
}
