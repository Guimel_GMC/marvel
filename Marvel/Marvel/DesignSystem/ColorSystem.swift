//
//  ColorSystem.swift
//  Marvel
//
//  Created by Guimel Moreno on 05/12/22.
//

import Foundation
import UIKit

extension UIColor {
    static let marvelBlack = UIColor.colorFromHex("#000000")
    static let marvelGray = UIColor.colorFromHex("#262525")
    static let marvelWhite = UIColor.colorFromHex("#FFFFFF")
    static let marvelRed = UIColor.colorFromHex("#E62429")
}
