//
//  ContentDetailModule.swift
//  Marvel
//
//  Created by Guimel Moreno on 06/12/22.
//  

import Foundation
import UIKit

final class ContentDetailModule {
    private let presenter: ContentDetailPresenterProtocol
    
    init(baseViewController: UIViewController, content: ContentModel) {
        let interactor = ContentDetailInteractor(content: content)
        let router = ContentDetailRouter(baseViewController: baseViewController)
        presenter = ContentDetailPresenter(interactor: interactor, router: router)
    }
    
    func showScreen() {
        return presenter.showScreen()
    }
}
