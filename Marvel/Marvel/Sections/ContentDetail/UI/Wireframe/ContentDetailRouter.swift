//
//  ContentDetailRouter.swift
//  Marvel
//
//  Created by Guimel Moreno on 06/12/22.
//  

import Foundation
import UIKit

protocol ContentDetailRouterProtocol {
    func showScreen(presenter: ContentDetailPresenterProtocol)
}

class ContentDetailRouter {
    private weak var baseController: UIViewController?
    private weak var module: ContentDetailViewController?
    
    init(baseViewController: UIViewController) {
        baseController = baseViewController
    }
}

extension ContentDetailRouter: ContentDetailRouterProtocol {
    func showScreen(presenter: ContentDetailPresenterProtocol) {
        guard let controller = baseController as? UINavigationController else { return }
        let viewController = ContentDetailViewController(presenter: presenter)
        controller.pushViewController(viewController, animated: true)
    }
}
