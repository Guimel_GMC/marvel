//
//  ContentDetailPresenter.swift
//  Marvel
//
//  Created by Guimel Moreno on 06/12/22.
//  

import Foundation
import UIKit

protocol ContentDetailPresenterProtocol {
    var content: ContentModel { get }
    func showScreen()
    func setViewProtocol(view: ContentDetailViewControllerProtocol)
}

class ContentDetailPresenter {
    private weak var viewProtocol: ContentDetailViewControllerProtocol?
    private let interactor: ContentDetailInteractorProtocol
    private let router: ContentDetailRouterProtocol
    
    init(interactor: ContentDetailInteractorProtocol, router: ContentDetailRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }
}

extension ContentDetailPresenter: ContentDetailPresenterProtocol {
    var content: ContentModel {
        interactor.content
    }
    
    func showScreen() {
        return router.showScreen(presenter: self)
    }
    
    func setViewProtocol(view: ContentDetailViewControllerProtocol) {
        viewProtocol = view
    }
}
