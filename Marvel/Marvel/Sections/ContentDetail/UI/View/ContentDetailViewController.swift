//
//  ContentDetailViewController.swift
//  Marvel
//
//  Created by Guimel Moreno on 06/12/22.
//  

import UIKit
import Kingfisher

protocol ContentDetailViewControllerProtocol: AnyObject {
    
}

class ContentDetailViewController: UIViewController {
    
    @IBOutlet private weak var imageView: UIImageView! {
        didSet {
            imageView.layer.cornerRadius = 4
            imageView.clipsToBounds = true
            imageView.contentMode = .scaleAspectFill
        }
    }
    @IBOutlet private weak var publishedLabel: UILabel! {
        didSet {
            publishedLabel.setTextStyle(.Style.bodyMedium)
            publishedLabel.text = "Published"
        }
    }
    @IBOutlet private weak var publishedDateLabel: UILabel! {
        didSet {
            publishedDateLabel.setTextStyle(.Style.bodyRegular)
            publishedDateLabel.numberOfLines = 0
            publishedDateLabel.adjustsFontSizeToFitWidth = true
        }
    }
    @IBOutlet private weak var writeLabel: UILabel! {
        didSet {
            writeLabel.setTextStyle(.Style.bodyMedium)
            writeLabel.text = "Writer"
        }
    }
    @IBOutlet private weak var writersLabel: UILabel! {
        didSet {
            writersLabel.setTextStyle(.Style.bodyRegular)
            writersLabel.numberOfLines = 0
            writersLabel.adjustsFontSizeToFitWidth = true
        }
    }
    @IBOutlet private weak var pencillerLabel: UILabel! {
        didSet {
            pencillerLabel.setTextStyle(.Style.bodyMedium)
            pencillerLabel.text = "Penciller"
        }
    }
    @IBOutlet private weak var pencillersLabel: UILabel!{
        didSet {
            pencillersLabel.setTextStyle(.Style.bodyRegular)
            pencillersLabel.numberOfLines = 0
            pencillersLabel.adjustsFontSizeToFitWidth = true
        }
    }
    @IBOutlet private weak var titleLabel: UILabel! {
        didSet {
            titleLabel.setTextStyle(.Style.headingsH3)
            titleLabel.numberOfLines = 0
        }
    }
    @IBOutlet private weak var contentTextView: UITextView! {
        didSet {
            contentTextView.backgroundColor = .marvelBlack
            contentTextView.setTextStyle(.Style.bodyRegular)
            contentTextView.isEditable = false
            contentTextView.isSelectable = false
        }
    }
    
    private let presenter: ContentDetailPresenterProtocol
    
    init(presenter: ContentDetailPresenterProtocol) {
        self.presenter = presenter
        super.init(nibName: (presenter.content.headerType == .comics) ? "ComicDetailViewController" : "SerieDetailViewController",
                   bundle: Bundle.main)
        self.presenter.setViewProtocol(view: self)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    private func configureView() {
        view.backgroundColor = .marvelBlack
        if presenter.content.headerType == .comics {
            publishedDateLabel.text = presenter.content.lastModified
            writersLabel.text =  presenter.content.creators?.writers.joined(separator: ", ") ?? "-"
            pencillersLabel.text = presenter.content.creators?.pencillers.joined(separator: ", ") ?? "-"
        }
        titleLabel.text = presenter.content.title
        contentTextView.text = presenter.content.description
        imageView.kf.setImage(with: presenter.content.thumbnail?.fullPath)
    }
}

extension ContentDetailViewController: ContentDetailViewControllerProtocol {
    
}
