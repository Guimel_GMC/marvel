//
//  ContentDetailInteractor.swift
//  Marvel
//
//  Created by Guimel Moreno on 06/12/22.
//  

import Foundation

protocol ContentDetailInteractorProtocol {
    var content: ContentModel { get }
}

class ContentDetailInteractor {
//    private let dataManager: ContentDetailDataManagerProtocol
    private let currentContent: ContentModel
    
    init(content: ContentModel) {
        self.currentContent = content
    }
}

extension ContentDetailInteractor: ContentDetailInteractorProtocol {
    var content: ContentModel {
        currentContent
    }
    
}

