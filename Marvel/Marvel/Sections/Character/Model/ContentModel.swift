//
//  ContentModel.swift
//  Marvel
//
//  Created by Guimel Moreno on 03/12/22.
//

import Foundation

struct ContentModel: Codable, Hashable {
    let id: Int
    let title: String
    let description: String?
    private var modified: String?
    var lastModified: String? {
        return modified?.capitalized
    }
    let urls: [UrlModel]?
    let prices: [PriceModel]?
    let thumbnail: ThumbnailModel?
    let creators: Creators?
    var headerType: HeaderType?
    
    private enum CodingKeys: String, CodingKey {
        case id
        case title
        case description
        case urls
        case prices
        case thumbnail
        case creators
        case modified
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decodeIfPresent(Int.self, forKey: .id) ?? -1
        self.title = try container.decodeIfPresent(String.self, forKey: .title) ?? ""
        self.description = try container.decodeIfPresent(String.self, forKey: .description)
        self.urls = try container.decodeIfPresent([UrlModel].self, forKey: .urls)
        self.prices = try container.decodeIfPresent([PriceModel].self, forKey: .prices)
        self.thumbnail = try container.decodeIfPresent(ThumbnailModel.self, forKey: .thumbnail)
        self.creators = try container.decodeIfPresent(Creators.self, forKey: .creators)
        self.modified = try container.decodeIfPresent(String.self, forKey: .modified)
        
        self.getLasModified()
    }
    
    mutating func getLasModified() {
        guard let lastDate = modified else { return }
        let iso8601 = ISO8601DateFormatter()
        let date = iso8601.date(from: lastDate)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM dd, yyyy"
        formatter.locale = Locale.current
        formatter.timeZone = TimeZone.current
        
        modified = formatter.string(from: date ?? Date())
    }
    
    public func hash(into hasher: inout Hasher) {
        return hasher.combine(id)
    }
    
    static func == (lhs: ContentModel, rhs: ContentModel) -> Bool {
        lhs.id == rhs.id
    }
}


