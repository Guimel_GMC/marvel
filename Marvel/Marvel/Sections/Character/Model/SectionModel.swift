//
//  SectionModel.swift
//  Marvel
//
//  Created by Guimel Moreno on 06/12/22.
//

import Foundation

enum HeaderType: Hashable {
    case principal
    case comics
    case series
    
    func getSectionTitle() -> String {
        switch self {
        case .principal: return ""
        case .series: return "Series"
        case .comics: return "Comics"
        }
    }
}

struct SectionModel: Hashable {
    
    var id: Int
    var headerType: HeaderType
    var content: [ContentModel]?
    
    public func hash(into hasher: inout Hasher) {
        return hasher.combine(id)
    }
    
    static func == (lhs: SectionModel, rhs: SectionModel) -> Bool {
        lhs.id == rhs.id
    }
}
