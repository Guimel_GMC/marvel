//
//  CharacterDataManager.swift
//  Marvel
//
//  Created by Guimel Moreno on 03/12/22.
//  

import Foundation

protocol CharacterDataManagerProtocol {
    func requestToGetComicsWith(idCharacter: Int, onSuccess: @escaping (CharacterModel) -> Void, onError: @escaping (RequestError) -> Void)
    func requestToGetContentInformation(endpoint: String, onSuccess: @escaping ([ContentModel]) -> Void, onError: @escaping (RequestError) -> Void)
//    func requestToGetSeriesInformation(endpoint: String, onSuccess: @escaping ([cont]) -> Void, onError: @escaping (RequestError) -> Void)
}

class CharacterDataManager: RequestManager {
    
}

extension CharacterDataManager: CharacterDataManagerProtocol {
    
    func requestToGetComicsWith(idCharacter: Int, onSuccess: @escaping (CharacterModel) -> Void, onError: @escaping (RequestError) -> Void) {
        let endpoint = Services.characterBy(idCharacter)
        
        makeRequest(.get, model: SuccessResponse<CharacterModel>.self, endpoint: endpoint, successRequest: { response in
            guard let data = response.data?.results.first else {
                onError(RequestError(code: .unknown))
                return
            }
            onSuccess(data)
        }, errorRequest: onError)
    }
    
    func requestToGetContentInformation(endpoint: String, onSuccess: @escaping ([ContentModel]) -> Void, onError: @escaping (RequestError) -> Void) {
        
        let params = ["offset": 0, "limit": 20]
        
        makeRequest(.get, model: SuccessResponse<ContentModel>.self, endpoint: endpoint, parameters: params, successRequest: { response in
            guard let results = response.data?.results else {
                onError(RequestError(code: .unknown))
                return
            }
            onSuccess(results)
        }, errorRequest: onError)
    }
    
//    func requestToGetSeriesInformation(endpoint: String, onSuccess: @escaping ([SerieModel]) -> Void, onError: @escaping (RequestError) -> Void) {
//
//        let params = ["offset": 0, "limit": 20]
//
//        makeRequest(.get, model: SuccessResponse<SerieModel>.self, endpoint: endpoint, parameters: params, successRequest: { response in
//            guard let results = response.data?.results else {
//                onError(RequestError(code: .unknown))
//                return
//            }
//            onSuccess(results)
//        }, errorRequest: onError)
//    }
}
