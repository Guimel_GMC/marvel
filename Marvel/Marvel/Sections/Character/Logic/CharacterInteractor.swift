//
//  CharacterInteractor.swift
//  Marvel
//
//  Created by Guimel Moreno on 03/12/22.
//  

import Foundation

protocol CharacterInteractorProtocol {
    var character: CharacterModel? { get }
    var sections: [SectionModel] { get }
    func fetchToGetCharacterDetail(onSuccess: @escaping () -> Void, onError: @escaping (String) -> Void)
}

class CharacterInteractor {
    private let dataManager: CharacterDataManagerProtocol
    private let idCharacter: Int
    private var currentCharacter: CharacterModel? {
        didSet {
            currentSections.append(SectionModel(id: 0, headerType: .principal))
        }
    }
    private var currentComics: [ContentModel]? {
        didSet {
            var section = SectionModel(id: 1, headerType: .comics)
            section.content = currentComics
            currentSections.append(section)
        }
    }
    private var currentSeries: [ContentModel]? {
        didSet {
            var section = SectionModel(id: 2, headerType: .series)
            section.content = currentSeries
            currentSections.append(section)
        }
    }
    
    var currentSections: [SectionModel] = [] {
        didSet {
            currentSections = currentSections.sorted(by: { $0.id < $1.id })
        }
    }
    
    init(dataManager: CharacterDataManagerProtocol, idCharacter: Int) {
        self.dataManager = dataManager
        self.idCharacter = idCharacter
    }
    
    private func fetchToNextServices(onFinish: @escaping () -> Void) {
        let dispatchGroup = DispatchGroup()
        
        if let endpointComic = currentCharacter?.comics?.collectionURI {
            dispatchGroup.enter()
            dataManager.requestToGetContentInformation(endpoint: endpointComic) { [weak self] comics in
                var allComics: [ContentModel] = []
                for var comic in comics {
                    comic.headerType = .comics
                    allComics.append(comic)
                }
                self?.currentComics = allComics
                dispatchGroup.leave()
            } onError: { error in
                debugPrint(error.localizedError)
                dispatchGroup.leave()
            }
        }
        
        if let endpointSeries = currentCharacter?.series?.collectionURI {
            dispatchGroup.enter()
            dataManager.requestToGetContentInformation(endpoint: endpointSeries) { [weak self] series in
                var allSeries: [ContentModel] = []
                for var serie in series {
                    serie.headerType = .series
                    allSeries.append(serie)
                }
                self?.currentSeries = allSeries
                dispatchGroup.leave()
            } onError: { error in
                debugPrint(error.localizedError)
                dispatchGroup.leave()
            }
        }        
        dispatchGroup.notify(queue: .main) {
            onFinish()
        }
    }
}

extension CharacterInteractor: CharacterInteractorProtocol {
    var sections: [SectionModel] {
        return currentSections
    }
    
    var character: CharacterModel? {
        currentCharacter
    }
    
    
    func fetchToGetCharacterDetail(onSuccess: @escaping () -> Void, onError: @escaping (String) -> Void) {
        dataManager.requestToGetComicsWith(idCharacter: idCharacter) { [weak self] response in
            self?.currentCharacter = response
            self?.fetchToNextServices(onFinish: onSuccess)
        } onError: { error in
            onError(error.localizedError)
        }
    }
    
}

