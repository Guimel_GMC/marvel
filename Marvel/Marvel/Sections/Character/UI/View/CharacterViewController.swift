//
//  CharacterViewController.swift
//  Marvel
//
//  Created by Guimel Moreno on 03/12/22.
//  

import UIKit

protocol CharacterViewControllerProtocol: AnyObject {
    func updateView()
}

class CharacterViewController: UIViewController {
    
    private lazy var collectionView: UICollectionView = {
        let collection = UICollectionView(frame: .zero,
                                          collectionViewLayout: UICollectionViewLayout())
        collection.backgroundColor = .marvelBlack
        collection.translatesAutoresizingMaskIntoConstraints = false
        collection.delegate = self
        return collection
    }()
    private var dataSource: UICollectionViewDiffableDataSource<SectionModel, ContentModel>?
    private let presenter: CharacterPresenterProtocol
    
    init(presenter: CharacterPresenterProtocol) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
        self.presenter.setViewProtocol(view: self)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    private func configureView() {
        view.addSubview(collectionView)
        view.backgroundColor = .marvelBlack
        collectionView.collectionViewLayout = createCompositionalLayout()
        
        collectionView.register(MVHeaderCharacter.self,
                                forSupplementaryViewOfKind: MVHeaderCharacter.elementKind,
                                withReuseIdentifier: MVHeaderCharacter.reuseIdentifier)
        
        collectionView.register(MVHeaderText.self,
                                forSupplementaryViewOfKind: MVHeaderText.elementKind,
                                withReuseIdentifier: MVHeaderText.reuseIdentifier)
        
        collectionView.register(MVComicCell.self,
                                forCellWithReuseIdentifier: MVComicCell.reuseIdentifier)
        
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: view.topAnchor),
            collectionView.leftAnchor.constraint(equalTo: view.leftAnchor),
            collectionView.rightAnchor.constraint(equalTo: view.rightAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        ])
        showLoadingView()
        presenter.prepareToGetCharacterDetail()
    }
}

extension CharacterViewController: CharacterViewControllerProtocol {
    func updateView() {
        createDataSource()
        reloadData()
        dismissLoading()
    }
}

private extension CharacterViewController {
    
    func configure<T: MVComicCellProtocol>(_ cellType: T.Type, with content: ContentModel, for indexPath: IndexPath) -> T {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellType.reuseIdentifier, for: indexPath) as? T else {
            fatalError("Cell not found")
        }
        
        cell.configureWith(content: content)
        return cell
    }
    
    func configurePrincipalHeader(collection: UICollectionView, kind: String, indexPath: IndexPath) -> UICollectionReusableView? {
        guard let header = collection.dequeueReusableSupplementaryView(ofKind: kind,
                                                                       withReuseIdentifier: MVHeaderCharacter.reuseIdentifier,
                                                                       for: indexPath) as? MVHeaderCharacter else { return nil }
        header.setupHeader(character: presenter.character, delegate: self)
        return header
    }

    func configureTextHeader(collection: UICollectionView, kind: String, indexPath: IndexPath) -> UICollectionReusableView? {
        guard let header = collection.dequeueReusableSupplementaryView(ofKind: kind,
                                                                       withReuseIdentifier: MVHeaderText.reuseIdentifier,
                                                                       for: indexPath) as? MVHeaderText else { return nil }
        let section = presenter.sections[indexPath.section].headerType.getSectionTitle()
        header.configureWith(title: section)
        return header
    }
    
    func createDataSource() {
        dataSource = UICollectionViewDiffableDataSource(collectionView: collectionView, cellProvider: { [weak self] collectionView, indexPath, item in
            guard let section = self?.presenter.sections[indexPath.section].headerType else { return UICollectionViewCell() }
            switch section {
            case .principal: return UICollectionViewCell()
            case .comics: return self?.configure(MVComicCell.self, with: item, for: indexPath)
            case .series: return self?.configure(MVComicCell.self, with: item, for: indexPath)
            }
        })
        
        dataSource?.supplementaryViewProvider = { [weak self] collection, kind, indexPath in
            if #available(iOS 15.0, *) {
                guard let section = self?.dataSource?.sectionIdentifier(for: indexPath.section) else { return nil }
                switch section.headerType {
                case .principal: return self?.configurePrincipalHeader(collection: collection, kind: kind, indexPath: indexPath)
                case .comics, .series: return self?.configureTextHeader(collection: collection, kind: kind, indexPath: indexPath)
                }
                
            } else {
                guard let section = self?.presenter.sections[indexPath.section] else { return nil }
                switch section.headerType {
                case .principal: return self?.configurePrincipalHeader(collection: collection, kind: kind, indexPath: indexPath)
                case .comics, .series: return self?.configureTextHeader(collection: collection, kind: kind, indexPath: indexPath)
                }
            }
        }
    }
    
    func reloadData() {
        var snapshot = NSDiffableDataSourceSnapshot<SectionModel, ContentModel>()
        snapshot.appendSections(presenter.sections)
        
        for section in presenter.sections {
            snapshot.appendItems(section.content ?? [], toSection: section)
        }
        
        dataSource?.apply(snapshot)
    }
    
    func createCompositionalLayout() -> UICollectionViewLayout {
        let layout = UICollectionViewCompositionalLayout { sectionIndex, layoutEnviroment in
            return self.createCollectionLayoutSection(index: sectionIndex)
        }
        
        let config = UICollectionViewCompositionalLayoutConfiguration()
        layout.configuration = config
        
        return layout
    }
    
    func createCollectionLayoutSection(index: Int) -> NSCollectionLayoutSection {
        
        if index == 0 {
            let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .absolute(0))
            let layoutItem = NSCollectionLayoutItem(layoutSize: itemSize)
            
            let layoutGroupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .absolute(0))
            let layoutGroup = NSCollectionLayoutGroup.horizontal(layoutSize: layoutGroupSize, subitems: [layoutItem])
            
            let layoutSection = NSCollectionLayoutSection(group: layoutGroup)
            
            let headerSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .absolute(329.0))
            let layoutSectionHeader = NSCollectionLayoutBoundarySupplementaryItem(layoutSize: headerSize,
                                                                                  elementKind: MVHeaderCharacter.elementKind,
                                                                                  alignment: .top)
            layoutSection.boundarySupplementaryItems = [layoutSectionHeader]
            return layoutSection
        } else {
            let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1/3), heightDimension: .fractionalHeight(1.0))
            let layoutItem = NSCollectionLayoutItem(layoutSize: itemSize)
            layoutItem.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 6, bottom: 0, trailing: 6)
            
            let layoutGroupSize: NSCollectionLayoutSize
            if index == 1 {
                layoutGroupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .absolute(234))
            } else {
                layoutGroupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .absolute(160))
            }
            let layoutGroup = NSCollectionLayoutGroup.horizontal(layoutSize: layoutGroupSize, subitems: [layoutItem])
            
            let headerTextSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .absolute(70.0))
            let layoutHeaderText = NSCollectionLayoutBoundarySupplementaryItem(layoutSize: headerTextSize,
                                                                               elementKind: MVHeaderText.elementKind,
                                                                               alignment: .top)
            let layoutSection = NSCollectionLayoutSection(group: layoutGroup)
            layoutSection.orthogonalScrollingBehavior = .continuous
            layoutSection.boundarySupplementaryItems = [layoutHeaderText]
            return layoutSection
        }
    }
}

extension CharacterViewController: MVHeaderCharacterDelegate {
    func openWebBrowserWith(url: URL?) {
        presenter.prepareForBrowserWith(url: url)
    }
}

extension CharacterViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter.prepareToContentDetail(indexPath: indexPath)
    }
}
