//
//  CharacterRouter.swift
//  Marvel
//
//  Created by Guimel Moreno on 03/12/22.
//  

import Foundation
import UIKit

protocol CharacterRouterProtocol {
    func showScreen(presenter: CharacterPresenterProtocol)
    func showAlertWith(message: String)
    func openBrowserAppWith(url: URL?)
    func showContentDetailModule(content: ContentModel)
}

class CharacterRouter {
    private weak var baseController: UIViewController?
    private weak var module: CharacterViewController?
    
    init(baseViewController: UIViewController) {
        baseController = baseViewController
    }
}

extension CharacterRouter: CharacterRouterProtocol {
    func showScreen(presenter: CharacterPresenterProtocol) {
        guard let controller = baseController as? UINavigationController else { return }
        let viewController = CharacterViewController(presenter: presenter)
        controller.pushViewController(viewController, animated: true)
    }
    
    func showAlertWith(message: String) {
        let alert = UIAlertController(title: "Marvel", message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Aceptar", style: .default)
        alert.addAction(okAction)
        baseController?.present(alert, animated: true)
    }
    
    func openBrowserAppWith(url: URL?) {
        guard let webpage = url, UIApplication.shared.canOpenURL(webpage) else { return }
        
        UIApplication.shared.open(webpage)
    }
    
    func showContentDetailModule(content: ContentModel) {
        guard let base = baseController else { return }
        let module = ContentDetailModule(baseViewController: base, content: content)
        module.showScreen()
    }
}
