//
//  CharacterPresenter.swift
//  Marvel
//
//  Created by Guimel Moreno on 03/12/22.
//  

import Foundation
import UIKit

protocol CharacterPresenterProtocol {
    var character: CharacterModel? { get }
    var sections: [SectionModel] { get }
    
    func showScreen()
    func setViewProtocol(view: CharacterViewControllerProtocol)
    
    func prepareToGetCharacterDetail()
    func prepareForBrowserWith(url: URL?)
    func prepareToContentDetail(indexPath: IndexPath)
}

class CharacterPresenter {
    private weak var viewProtocol: CharacterViewControllerProtocol?
    private let interactor: CharacterInteractorProtocol
    private let router: CharacterRouterProtocol
    
    init(interactor: CharacterInteractorProtocol, router: CharacterRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }
}

extension CharacterPresenter: CharacterPresenterProtocol {
    var sections: [SectionModel] {
        interactor.sections
    }
    
    var character: CharacterModel? {
        interactor.character
    }
    
    func showScreen() {
        return router.showScreen(presenter: self)
    }
    
    func setViewProtocol(view: CharacterViewControllerProtocol) {
        viewProtocol = view
    }
    
    func prepareToGetCharacterDetail() {
        interactor.fetchToGetCharacterDetail {
            DispatchQueue.main.async { [weak self] in
                self?.viewProtocol?.updateView()
            }
        } onError: { error in
            DispatchQueue.main.async { [weak self] in
                self?.router.showAlertWith(message: error)
            }
        }
    }
    
    func prepareForBrowserWith(url: URL?) {
        router.openBrowserAppWith(url: url)
    }
    
    func prepareToContentDetail(indexPath: IndexPath) {
        guard let content = sections[indexPath.section].content?[indexPath.row] else { return }
        router.showContentDetailModule(content: content)
    }
}
