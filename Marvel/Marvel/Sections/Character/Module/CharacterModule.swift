//
//  CharacterModule.swift
//  Marvel
//
//  Created by Guimel Moreno on 03/12/22.
//  

import Foundation
import UIKit

final class CharacterModule {
    private let presenter: CharacterPresenterProtocol
    
    init(baseViewController: UIViewController, idCharacter: Int) {
        let interactor = CharacterInteractor(dataManager: CharacterDataManager(), idCharacter: idCharacter)
        let router = CharacterRouter(baseViewController: baseViewController)
        presenter = CharacterPresenter(interactor: interactor, router: router)
    }
    
    func showScreen() {
        return presenter.showScreen()
    }
}
