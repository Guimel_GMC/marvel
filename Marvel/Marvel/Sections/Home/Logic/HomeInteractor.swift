//
//  HomeInteractor.swift
//  Marvel
//
//  Created by Guimel Moreno on 02/12/22.
//  

import Foundation

protocol HomeInteractorProtocol {
    var characterName: String? { get set }
    var characters: [CharacterModel] { get }
    func fetchCharacters(onSuccess: @escaping () -> Void, onError: @escaping (String) -> Void)
}

class HomeInteractor {
    private let dataManager: HomeDataManagerProtocol
    private var characterData: DataResponse<CharacterModel>? {
        didSet {
            limit = characterData?.limit ?? 0
            offset = (characterData?.offset ?? 0) + limit
            guard let results = characterData?.results else { return }
            
            if nameToSearch != nil {
                allCharacters = []
            }
            
            for char in results {
                if !allCharacters.contains(where: { char.id == $0.id }) {
                    allCharacters.append(char)
                }
            }
            
        }
    }
    private var offset: Int = 0
    private var total: Int {
        characterData?.total ?? 0
    }
    private var limit: Int = 30
    private var allCharacters: [CharacterModel] = []
    private var nameToSearch: String? {
        didSet {
            if let name = nameToSearch {
                nameToSearch = name.isEmpty ? nil : name
            } else {
                offset = 0
                allCharacters = []
            }
        }
    }
    
    init(dataManager: HomeDataManagerProtocol) {
        self.dataManager = dataManager
    }
}

extension HomeInteractor: HomeInteractorProtocol {
    
    var characters: [CharacterModel] {
        return allCharacters
    }
    
    var characterName: String? {
        get {
            return nameToSearch
        }
        set {
            nameToSearch = newValue
        }
    }
    
    func fetchCharacters(onSuccess: @escaping () -> Void, onError: @escaping (String) -> Void) {
        
        guard offset == 0 || (offset <= total && total > 1) else { return }
        
        if nameToSearch != nil {
            offset = 0
        }
        
        dataManager.requestToGetCharacters(offset: offset, limit: limit, nameCharacter: nameToSearch) { [weak self] response in
            self?.characterData = response
            onSuccess()
        } onError: { error in
            debugPrint(error)
            onError(error.localizedError)
        }
    }
}

