//
//  HomeDataManager.swift
//  Marvel
//
//  Created by Guimel Moreno on 02/12/22.
//  

import Foundation

protocol HomeDataManagerProtocol {
    func requestToGetCharacters(offset: Int, limit: Int, nameCharacter: String?, onSuccess: @escaping (DataResponse<CharacterModel>) -> Void, onError: @escaping (RequestError) -> Void)
}

class HomeDataManager: RequestManager {
    
}

extension HomeDataManager: HomeDataManagerProtocol {
    func requestToGetCharacters(offset: Int, limit: Int, nameCharacter: String?, onSuccess: @escaping (DataResponse<CharacterModel>) -> Void, onError: @escaping (RequestError) -> Void) {
        let endpoint = Services.characters
        var params: [String: Any] = ["limit": limit, "offset" : offset]
        if let name = nameCharacter {
            params.updateValue(name.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? "", forKey: "nameStartsWith")
        }
        makeRequest(.get, model: SuccessResponse<CharacterModel>.self, endpoint: endpoint,parameters: params, successRequest: { response in
            guard let data = response.data else {
                onError(RequestError(code: .unknown))
                return
            }
            onSuccess(data)
        }, errorRequest: onError)
    }
    
    
}
