//
//  HomeModule.swift
//  Marvel
//
//  Created by Guimel Moreno on 02/12/22.
//  

import Foundation
import UIKit

final class HomeModule {
    private let presenter: HomePresenterProtocol
    
    init(window: UIWindow) {
        let interactor = HomeInteractor(dataManager: HomeDataManager())
        let router = HomeRouter(window: window)
        presenter = HomePresenter(interactor: interactor, router: router)
    }
    
    func showScreen() {
        return presenter.showScreen()
    }
}
