//
//  HomePresenter.swift
//  Marvel
//
//  Created by Guimel Moreno on 02/12/22.
//  

import Foundation
import UIKit

protocol HomePresenterProtocol {
    var characterName: String? { get set }
    var characters: [CharacterModel] { get }
    func showScreen()
    func setViewProtocol(view: HomeViewControllerProtocol)
    func prepareToFetchCharacters()
    func prepareToCharacterDetail(indexPath: IndexPath)
}

class HomePresenter {
    private weak var viewProtocol: HomeViewControllerProtocol?
    private var interactor: HomeInteractorProtocol
    private let router: HomeRouterProtocol
    
    init(interactor: HomeInteractorProtocol, router: HomeRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }
}

extension HomePresenter: HomePresenterProtocol {
    var characters: [CharacterModel] {
        return interactor.characters
    }
    
    var characterName: String? {
        get {
            interactor.characterName
        }
        set {
            interactor.characterName = newValue
        }
    }
    
    func showScreen() {
        return router.showScreen(presenter: self)
    }
    
    func setViewProtocol(view: HomeViewControllerProtocol) {
        viewProtocol = view
    }
    
    func prepareToFetchCharacters() {
        interactor.fetchCharacters {
            DispatchQueue.main.async { [weak self] in
                self?.viewProtocol?.updateCollectionView()
            }
        } onError: { error in
            DispatchQueue.main.async { [weak self] in
                self?.router.showAlertWith(message: error)
                self?.viewProtocol?.updateCollectionView()
            }
        }
    }
    
    func prepareToCharacterDetail(indexPath: IndexPath) {
        router.showCharacterWith(idCharacter: characters[indexPath.item].id)
    }
}
