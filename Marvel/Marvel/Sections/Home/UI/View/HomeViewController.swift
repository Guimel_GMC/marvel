//
//  HomeViewController.swift
//  Marvel
//
//  Created by Guimel Moreno on 02/12/22.
//  

import UIKit

protocol HomeViewControllerProtocol: AnyObject {
    func updateCollectionView()
}

class HomeViewController: UIViewController {
    
    private lazy var collectionView: UICollectionView = {
        let collection = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewLayout())
        collection.backgroundColor = .marvelBlack
        collection.register(MVCharacterCell.self, forCellWithReuseIdentifier: MVCharacterCell.reuseIdentifier)
        collection.translatesAutoresizingMaskIntoConstraints = false
        collection.contentInset = UIEdgeInsets(top: 30, left: 0, bottom: 0, right: 0)
        collection.delegate = self
        return collection
    }()
    
    private lazy var searchController: UISearchController = {
        let controller = UISearchController(searchResultsController: nil)
        controller.searchBar.placeholder = "Search"
        controller.searchBar.tintColor = .marvelWhite
        controller.obscuresBackgroundDuringPresentation = false
        controller.automaticallyShowsSearchResultsController = false
        controller.showsSearchResultsController = false
        controller.searchBar.delegate = self
        return controller
    }()
    
    private var dataSource: UICollectionViewDiffableDataSource<CharacterModel, CharacterModel>?
    private var presenter: HomePresenterProtocol
    private var isLoading: Bool = false
    
    init(presenter: HomePresenterProtocol) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
        self.presenter.setViewProtocol(view: self)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    private func configureView() {
        let titleView = UIImageView(image: Style.iconCatalog.marvenLogo)
        titleView.contentMode = .scaleAspectFit
        navigationItem.titleView = titleView
        navigationItem.searchController = searchController
        
        view.addSubview(collectionView)
        view.backgroundColor = .marvelBlack
        
        isLoading.toggle()
        collectionView.collectionViewLayout = createCompositionalLayout()
        
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: view.topAnchor),
            collectionView.leftAnchor.constraint(equalTo: view.leftAnchor),
            collectionView.rightAnchor.constraint(equalTo: view.rightAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            titleView.heightAnchor.constraint(equalToConstant: 30)
        ])
        
        showLoadingView()
        presenter.prepareToFetchCharacters()
    }
}

extension HomeViewController: HomeViewControllerProtocol {
    func updateCollectionView() {
        createDataSource()
        reloadData()
        isLoading = false
        dismissLoading()
    }
}

private extension HomeViewController {
        
    func configure<T: MVCharacterCellProtocol>(_ cellType: T.Type, with character: CharacterModel, for indexPath: IndexPath) -> T {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellType.reuseIdentifier, for: indexPath) as? T else {
            fatalError("Cell not found")
        }
        cell.configureCellWith(character: character)
        return cell
    }
    
    func createDataSource() {
        dataSource = UICollectionViewDiffableDataSource(collectionView: collectionView, cellProvider: { [weak self] collectionView, indexPath, item in
            return self?.configure(MVCharacterCell.self, with: item, for: indexPath)
        })
    }

    func reloadData() {
        guard !presenter.characters.isEmpty else { return }
        var snapshot = NSDiffableDataSourceSnapshot<CharacterModel, CharacterModel>()
        snapshot.appendSections(presenter.characters)
        snapshot.appendItems(presenter.characters)
        dataSource?.apply(snapshot, animatingDifferences: true)
    }
        
    func createCompositionalLayout() -> UICollectionViewLayout {
        let layout = UICollectionViewCompositionalLayout { sectionIndex, layoutEnviroment in
            return self.createCollectionLayoutSection()
        }
        
        let config = UICollectionViewCompositionalLayoutConfiguration()
        layout.configuration = config
        
        return layout
    }
    
    func createCollectionLayoutSection() -> NSCollectionLayoutSection {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1/2), heightDimension: .fractionalHeight(1.0))
        let layoutItem = NSCollectionLayoutItem(layoutSize: itemSize)
        layoutItem.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 8, bottom: 8, trailing: 8)
        
        let layoutGroupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalHeight(1/4))
        let layoutGroup = NSCollectionLayoutGroup.horizontal(layoutSize: layoutGroupSize, subitems: [layoutItem])
        
        let layoutSection = NSCollectionLayoutSection(group: layoutGroup)
        
        return layoutSection
    }
    
}

extension HomeViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter.prepareToCharacterDetail(indexPath: indexPath)
    }
    
}

extension HomeViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let maxOffset = scrollView.contentSize.height - scrollView.frame.size.height
        let valid = maxOffset - offsetY
        if valid <= -30 && !isLoading {
            presenter.prepareToFetchCharacters()
            isLoading.toggle()
        }
    }
}

extension HomeViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        presenter.characterName = searchBar.text
        showLoadingView()
        presenter.prepareToFetchCharacters()
        isLoading.toggle()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        presenter.characterName = nil
        showLoadingView()
        presenter.prepareToFetchCharacters()
        isLoading.toggle()
    }
}
