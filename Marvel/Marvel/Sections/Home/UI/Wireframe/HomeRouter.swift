//
//  HomeRouter.swift
//  Marvel
//
//  Created by Guimel Moreno on 02/12/22.
//  

import Foundation
import UIKit

protocol HomeRouterProtocol {
    func showScreen(presenter: HomePresenterProtocol)
    func showAlertWith(message: String)
    func showCharacterWith(idCharacter: Int)
}

class HomeRouter {
    private var window: UIWindow
    private weak var baseController: UIViewController?
    
    init(window: UIWindow) {
        self.window = window
    }
}

extension HomeRouter: HomeRouterProtocol {
    func showScreen(presenter: HomePresenterProtocol) {
        let viewController = HomeViewController(presenter: presenter)
        let navigation = UINavigationController(rootViewController: viewController)
        window.rootViewController = navigation
        baseController = navigation
    }
    
    func showAlertWith(message: String) {
        let alert = UIAlertController(title: "Marvel", message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Aceptar", style: .default)
        alert.addAction(okAction)
        baseController?.present(alert, animated: true)
    }
    
    func showCharacterWith(idCharacter: Int) {
        guard let navigation = baseController as? UINavigationController else { return }
        let module = CharacterModule(baseViewController: navigation, idCharacter: idCharacter)
        module.showScreen()
    }
}
