//
//  CharacterModel.swift
//  Marvel
//
//  Created by Guimel Moreno on 02/12/22.
//

import Foundation

struct CharacterModel: Codable, Hashable {
    
    let id: Int
    let name: String?
    let description: String?
    let thumbnail: ThumbnailModel?
    let comics: ResourceCharacter?
    let series: ResourceCharacter?
    let stories: ResourceCharacter?
    let events: ResourceCharacter?
    let urls: [UrlModel]?
    
    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case thumbnail
        case description
        case comics
        case series
        case stories
        case events
        case urls
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decodeIfPresent(Int.self, forKey: .id) ?? -1
        name = try container.decodeIfPresent(String.self, forKey: .name)
        description = try container.decodeIfPresent(String.self, forKey: .description)
        thumbnail = try container.decodeIfPresent(ThumbnailModel.self, forKey: .thumbnail)
        comics = try container.decodeIfPresent(ResourceCharacter.self, forKey: .comics)
        series = try container.decodeIfPresent(ResourceCharacter.self, forKey: .series)
        stories = try container.decodeIfPresent(ResourceCharacter.self, forKey: .stories)
        events = try container.decodeIfPresent(ResourceCharacter.self, forKey: .events)
        urls = try container.decodeIfPresent([UrlModel].self, forKey: .urls)
    }
    
    public func hash(into hasher: inout Hasher) {
        return hasher.combine(id)
    }
    
    static func == (lhs: CharacterModel, rhs: CharacterModel) -> Bool {
        lhs.id == rhs.id
    }
}

struct ThumbnailModel: Codable {
    private var path: String?
    private var fileExtension: String?
    var fullPath: URL? {
        return URL(string: "\(path ?? "").\(fileExtension ?? "")")
    }
    
    
    private enum CodingKeys: String, CodingKey {
        case path
        case fileExtension = "extension"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.path = try container.decodeIfPresent(String.self, forKey: .path)
        self.fileExtension = try container.decodeIfPresent(String.self, forKey: .fileExtension)
    }
}

struct ResourceCharacter: Codable {
    let collectionURI: String?
    
    private enum CodingKeys: String, CodingKey {
        case collectionURI
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.collectionURI = try container.decodeIfPresent(String.self, forKey: .collectionURI)
    }
}
