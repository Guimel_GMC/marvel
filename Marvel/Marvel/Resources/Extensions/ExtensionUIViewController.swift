//
//  ExtensionUIViewController.swift
//  Marvel
//
//  Created by Guimel Moreno on 05/12/22.
//

import Foundation
import UIKit
import IHProgressHUD

extension UIViewController {
    func showLoadingView(status: String? = nil) {
        IHProgressHUD.show(withStatus: status)
    }
    
    func dismissLoading() {
        IHProgressHUD.dismiss()
    }
}
