//
//  RequestError.swift
//  Marvel
//
//  Created by Guimel Moreno on 03/12/22.
//

import Foundation
import Alamofire

enum CodeResponse: Int {
    case missingApiKeyHashOrTimestamp = 409
    case invalidRefererOrHash = 401
    case methodNotAllowed = 405
    case forbidden = 403
    case unknown = -1
    
    func getMessage() -> String {
        switch self {
        case .missingApiKeyHashOrTimestamp:
            return "Occurs when an apikey parameter is included with a request, a hash parameter is present, but no ts parameter is sent. Occurs on server-side applications only."
        case .invalidRefererOrHash:
            return "Occurs when a referrer which is not valid for the passed apikey parameter is sent. Occurs when a ts, hash and apikey parameter are sent but the hash is not valid per the above hash generation rule."
        case .methodNotAllowed:
            return "Occurs when an API endpoint is accessed using an HTTP verb which is not allowed for that endpoint."
        case .forbidden:
            return "Occurs when a user with an otherwise authenticated request attempts to access an endpoint to which they do not have access."
        case .unknown:
            return "Unknown error"
        }
    }
}

struct RequestError {
    var localizedError: String
    var code: CodeResponse
    
    init(from error: AFError) {
        code = CodeResponse(rawValue: error.responseCode ?? -1) ?? .unknown
        localizedError = code.getMessage()
    }
    
    init(code: CodeResponse) {
        self.code = code
        localizedError = code.getMessage()
    }
}
