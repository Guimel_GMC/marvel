//
//  UrlModel.swift
//  Marvel
//
//  Created by Guimel Moreno on 04/12/22.
//

import Foundation

enum URLType: String, Codable {
    case purchase
    case detail
    case reader
    case inAppLink
    case wiki
    case comiclink
    case none
}

struct UrlModel: Codable {
    let type: URLType
    let url: String?
    
    private enum CodingKeys: String, CodingKey {
        case type
        case url
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.url = try container.decodeIfPresent(String.self, forKey: .url)
        if url == nil {
            type = .none
        } else {
            type = try container.decodeIfPresent(URLType.self, forKey: .type) ?? .none
        }
    }
}
