//
//  HomeDataResponse.swift
//  Marvel
//
//  Created by Guimel Moreno on 02/12/22.
//

import Foundation

class SuccessResponse<T: Codable>: Codable {
    var code: Int
    var data: DataResponse<T>?
    
    private enum CodingKeys: String, CodingKey {
        case code
        case data
    }
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.code = try container.decodeIfPresent(Int.self, forKey: .code) ?? -1
        self.data = try container.decodeIfPresent(DataResponse.self, forKey: .data)
    }
}

class DataResponse<T: Codable>: Codable {
    var offset: Int
    var limit: Int
    var total: Int
    var count: Int
    var results: [T]
    
    enum CodingKeys: String, CodingKey {
        case offset
        case limit
        case total
        case count
        case results
    }
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.offset = try container.decodeIfPresent(Int.self, forKey: .offset) ?? -1
        self.limit = try container.decodeIfPresent(Int.self, forKey: .limit) ?? -1
        self.total = try container.decodeIfPresent(Int.self, forKey: .total) ?? -1
        self.count = try container.decodeIfPresent(Int.self, forKey: .count) ?? -1
        self.results = try container.decodeIfPresent([T].self, forKey: .results) ?? []
    }
}
