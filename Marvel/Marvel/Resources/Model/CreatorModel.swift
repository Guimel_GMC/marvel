//
//  CreatorModel.swift
//  Marvel
//
//  Created by Guimel Moreno on 04/12/22.
//

import Foundation

//enum RoleType: String, Codable {
//    case editor
//    case letterer
//    case penciller
//    case penciler
//    case percillerCover = "penciller (cover)"
//    case percilerCover = "penciler (cover)"
//    case writer
//    case Writer = "Writer"
//    case colorist
//    case inker
//    case inkerCover = "inker (cover)"
//    case other
//    case artist
//    case coloristCover = "colorist (cover)"
//    case paiterCover = "painter (cover)"
//    case none
//
//    func getDescription() -> String {
//        switch self {
//        case .editor: return "Editor"
//        case .letterer: return "Letterer"
//        case .penciller: return "Penciller"
//        case .penciler: return "Penciler"
//        case .percillerCover: return "Penciller (Cover)"
//        case .percilerCover: return "Penciler (cover)"
//        case .writer, .Writer: return "Writer"
//        case .colorist: return "Colorist"
//        case .inker: return "Inker"
//        case .inkerCover: return "Inker (Cover)"
//        case .paiterCover: return "Painter (cover)"
//        case .other:return "Other"
//        case .artist: return "Artist"
//        case .coloristCover: return "Colorist (cover)"
//        case .none:
//            return "None"
//        }
//    }
//}

struct CreatorModel: Codable {
    let name: String?
    var role: String? {
        didSet {
            role = (role?.prefix(1).capitalized ?? "") + (role?.dropFirst().lowercased() ?? "")
        }
    }
    
    private enum CodingKeys: String, CodingKey {
        case name
        case role
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.name = try container.decodeIfPresent(String.self, forKey: .name)
        self.role = try container.decodeIfPresent(String.self, forKey: .role)
    }
}

struct Creators: Codable {
    let items: [CreatorModel]
    var writers: [String] = []
    var pencillers: [String] = []
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.items = try container.decodeIfPresent([CreatorModel].self, forKey: .items) ?? []
        
        let writersFiltered = items.compactMap({ $0 }).filter({ $0.role?.lowercased().hasPrefix("writer") ?? false })
        writers.append(contentsOf: writersFiltered.map({ return $0.name ?? "" }))
        
        let pencillerFiltered = items.compactMap({ $0 }).filter({ $0.role?.lowercased().hasPrefix("penciller") ?? false })
        pencillers.append(contentsOf: pencillerFiltered.map({ return $0.name ?? "" }))
    }
}
