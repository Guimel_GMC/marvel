//
//  PriceModel.swift
//  Marvel
//
//  Created by Guimel Moreno on 04/12/22.
//

import Foundation

enum PriceType: String, Codable {
    case printPrice
    case digitalPurchasePrice
    case none
}

struct PriceModel: Codable {
    let type: PriceType
    let price: Double
    
    private enum CodingKeys: String, CodingKey {
        case type
        case price
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.type = try container.decodeIfPresent(PriceType.self, forKey: .type) ?? .none
        self.price = try container.decodeIfPresent(Double.self, forKey: .price) ?? 0
    }
}
