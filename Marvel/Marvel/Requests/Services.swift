//
//  Services.swift
//  Marvel
//
//  Created by Guimel Moreno on 02/12/22.
//

import Foundation

struct Services {
    static var characters: String {
        return String(format: "%@/v1/public/characters", getBaseURL())
    }
    
    static func characterBy(_ idCharacter: Int) -> String {
        return String(format: "%@/v1/public/characters/%02d", getBaseURL(), idCharacter)
    }
}

private extension Services {
    static func getBaseURL() -> String {
        return "https://gateway.marvel.com"
    }
}
