//
//  Request.swift
//  Marvel
//
//  Created by Guimel Moreno on 02/12/22.
//

import Foundation
import Alamofire

class RequestManager {
    
    func makeRequest<T: Decodable>(_ method: HTTPMethod,
                                   model: T.Type,
                                   endpoint: String,
                                   parameters: Parameters? = nil,
                                   encoding: ParameterEncoding = URLEncoding.default,
                                   successRequest: @escaping (T) -> Void,
                                   errorRequest: @escaping (RequestError) -> Void) {
        
//        let headers: HTTPHeaders = ["" : ""]
        var params: Parameters = parameters ?? [:]
        
        addSecurityParameters(current: &params)
        
        debugPrint("=== ENDPOINT ===> \(endpoint)")
        debugPrint("=== PARAMETERS ===> \(params.description)")
        
        AF.request(endpoint, method: method, parameters: params, encoding: encoding, headers: nil).responseDecodable(of: T.self) { response in
            if let error = response.error {
                errorRequest(RequestError(from: error))
                debugPrint("=== ¡¡¡ERROR!!! ===")
            }
            
            switch response.result {
            case .success(let modelResponse):
                successRequest(modelResponse)
            case .failure(let error):
                errorRequest(RequestError(from: error))
                debugPrint(error)
            }
        }
        
    }
    
}

private extension RequestManager {
    func addSecurityParameters(current parameters: inout Parameters) {
        parameters.updateValue(1, forKey: "ts")
        parameters.updateValue("6b50768643e41fea615483878e0d9e2b", forKey: "apikey")
        parameters.updateValue("e69adfbeec9e0a14bea6887a57a24592", forKey: "hash")
    }
}
