//
//  AppDependencies.swift
//  Marvel
//
//  Created by Guimel Moreno on 02/12/22.
//

import Foundation
import UIKit

class AppDependencies {
    var window: UIWindow?
    
    func initWith(window: UIWindow) {
        self.window = window
        installRootViewController()
    }
    
}

private extension AppDependencies {
    
    func installRootViewController() {
        guard let window = window else { fatalError("UIWindow failed in AppDependencies") }
        configureDependencies()
        let homeModule = HomeModule(window: window)
        homeModule.showScreen()
    }
    
    func configureDependencies() {
        configuraNavigationBar()
    }
    
    func configuraNavigationBar() {
        let navigationAppearance = UINavigationBarAppearance()
        navigationAppearance.configureWithOpaqueBackground()
        navigationAppearance.backgroundColor = .marvelBlack
        navigationAppearance.shadowImage = UIImage()
        navigationAppearance.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.marvelWhite]
        navigationAppearance.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.marvelWhite]
        navigationAppearance.setBackIndicatorImage(Style.iconCatalog.arrowBackward, transitionMaskImage: Style.iconCatalog.arrowBackward)
        navigationAppearance.buttonAppearance.normal.titlePositionAdjustment = UIOffset(horizontal: -300, vertical: 0)
        
        UINavigationBar.appearance().standardAppearance = navigationAppearance
        UINavigationBar.appearance().compactAppearance = navigationAppearance
        UINavigationBar.appearance().scrollEdgeAppearance = navigationAppearance
        UINavigationBar.appearance().tintColor = .marvelWhite
        
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.marvelWhite, NSAttributedString.Key.font: Typography.Style.bodySmall.asFont()]
        
    }
}
