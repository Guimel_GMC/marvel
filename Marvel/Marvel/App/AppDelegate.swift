//
//  AppDelegate.swift
//  Marvel
//
//  Created by Guimel Moreno on 02/12/22.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let dependencies = AppDependencies()
        window = UIWindow(frame: UIScreen.main.bounds)
        if let window = window {
            dependencies.initWith(window: window)
            window.makeKeyAndVisible()
        }
        
        return true
    }
}

